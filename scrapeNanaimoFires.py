#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 21:19:27 2021

scrape fire incident data from Nanaimo open data project:
    
    https://www.nanaimo.ca/your-government/maps-data/open-data-catalogue/open-data-catalogue-for-developers
    
Use pandas to analyse data

@author: andres
"""
#
import os

import urllib.request
import json
import sqlite3
from sqlalchemy import create_engine # to save a sqlite db 
import pandas as pd

import warnings

warnings.filterwarnings("ignore")

# read data feed from nanaimo city api
urlData = "https://api.nanaimo.ca/dataservice/v1/sql/FireDailyCalls/?format=json" # (fire incidents in Nanaimo, BC)
sqlite_table = "incidents"
db_filename = "nfincidents"

# load data from url and save it as sqlite file 
def getResponse(url):
    operUrl = urllib.request.urlopen(url)
    if(operUrl.getcode()==200):
        data = operUrl.read()
        jsonData = json.loads(data)
    else:
        print("Error receiving data", operUrl.getcode())
    return jsonData

def getDataFrame(url):
    jsonData = getResponse(url) # read json data from url
    df = pd.DataFrame(jsonData['d']) # json to dataframe
    return df
    
def preProcessIncidents(df):
    # incident_desrciption to categories
    df['Description'] = df['Description'].astype('category')
    # datetime column to index
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df.set_index('Timestamp', inplace=True)

def printInfo(df):
    print(df['Description'].describe()) # count, unique, top, freq
    print("\n\ttop call reason: {}\n".format(df['Description'].describe()['top']))
    print(df.info())
    print('\nfrom {}\n to {}'.format(df.index.min(), df.index.max()))
    print('{} calls received'.format(len(df.index)))

def main():
    df = getDataFrame(urlData)
    # work with incident description and time of incident
    printInfo(df)
    # if bd exists, read, merge and write otherwise write to sqlite
    if os.path.exists(os.path.join("./", db_filename+".db")) and os.path.isfile(os.path.join("./", db_filename+".db")):
        cnx = sqlite3.connect(os.path.join("./", db_filename+".db"))
        df0 = pd.read_sql_query("SELECT * FROM {}".format(sqlite_table), cnx, index_col="index") # to eliminate posible duplicates, use call_id column
        df = df.merge(df0, on=list(df.columns), how='outer')
        df.to_sql(sqlite_table, cnx, if_exists='replace')
    #
    else:# Create the connection using the imported create_engine function and then invoking the connect method on it
        engine = create_engine('sqlite:///'+db_filename+'.db', echo=True)
        sqlite_connection = engine.connect()
        df.to_sql(sqlite_table, sqlite_connection, if_exists='fail')
        sqlite_connection.close()
    # 
    preProcessIncidents(df)
    printInfo(df)

if __name__ == "__main__":
    main()
