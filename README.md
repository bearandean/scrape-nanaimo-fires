# Nanaimo Fire Calls Incidence

## tltr

Visit the [link](http://nanaimo-fire-incidence.herokuapp.com/) to learn more about Nanaimo fire calls incidence data.

An implementation of a *git-scraper* for gathering *fire incident data* from the city of Nanaimo in British Columbia, Canada. The code includes Python modules and *jupyter notebooks* to explore the data. It works by requesting data from the city of Nanaimo open data project urls, and saving it as an *sqlite .db* file. It updates this *sqlite* database file automatically using *Bitbucket's pipelines*.

### Data explorer

A *jupyter notebook* called *exploreIncidentData.ipynb* including an exploratory data analysis allows to understand some aspects behind the data itself. It can be [here](http://nanaimo-fire-incidence.herokuapp.com/).

### Git-Scraper

Makes use of *Bitbucket's* pipelines to update an *SQLite* database file (*nfincidents.db*) every 24 hours with the lastest incidents reported according to the city of Nanaimo open database. The requests are made via the API available in JSON format. For more information visit the [link](https://www.nanaimo.ca/open-data-catalogue/DataBrowser/nanaimo/FireDailyCalls#param=NOFILTER--DataView--Results).

## Motivation

After reading Simon Willison's [weblog entry](https://simonwillison.net/2020/Oct/9/git-scraping/) about git-scraping I decided to try my own data scraper using fire incidents data from [Nanaimo's open data catalogue](https://www.nanaimo.ca/your-government/maps-data/open-data-catalogue/open-data-catalogue-for-developers).

## Python module install

If you would like to run the code in the *Python* file *scrapeNanaimoFires.py*. Some preliminary steps must be followed.

### Pre-requisites

Python 3+.

### How to run it

Use available requirements file to install needed modules.
In a terminal or command line type:

```bash
python -m pip install -r requirements.txt
```

## Usage

As stand alone Python:

```bash
python scrapeNanaimoFires.py
```

For usage as Jupyter notebook:

```bash
jupyter notebook scrapeNanaimoFires.ipynb
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Credits

[City Of Nanaimo](https://www.nanaimo.ca/)

## License

[MIT](https://choosealicense.com/licenses/mit/)
